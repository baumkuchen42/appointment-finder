# AppointmentFinder

An application to find common appointments.

## How to use:

1. Enter a url and port where your server gets created
2. You can then create new windows via the submenu to test it with multiple clients or run the application with the same connection details on another machine

## Screenshots

### The setup page:

![image](/uploads/c8d3ea5e470a2b21c8e380f7463c25d2/image.png)

### Creating a new appointment:

![image](/uploads/d386f992f6d7acf9743a133e1969ccd5/image.png)

### Requesting a new common appointment:

![image](/uploads/658825615400a73a767423075b7c7513/image.png)
