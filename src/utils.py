import re, pandas

from gi.repository import Gtk
from datetime import date, time, timedelta, datetime

def get_formatted_date_from_calendar(calendar: Gtk.Calendar):
	year, month, day = calendar.get_date()
	return date(year, month+1, day)

def select_current_date(calendar: Gtk.Calendar):
	today = date.today()
	calendar.select_month(today.month-1, today.year)
	calendar.mark_day(today.day)

def is_valid_ip(ip:str):
	return re.match(r'[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', ip)

def is_valid_port(port):
	return re.match(r'[0-9]{1,5}', str(port)) and (0 < int(port) <= 65535)

class Appointment():
	def __init__(self, title, day: date, start_hour: int, start_minute: int, end_hour: int, end_minute: int):
		self.title = title
		self.day = day
		self.start_time = time(start_hour, start_minute)
		self.end_time = time(end_hour, end_minute)
		self.validate()

	def validate(self):
		if self.end_time < self.start_time:
			raise Exception(f'Start time {self.start_time} is after end time {self.end_time}.')

	def get_start_hour(self):
		return self.start_time.hour

	def get_start_minute(self):
		return self.start_time.minute

	def get_end_hour(self):
		return self.end_time.hour

	def get_start_minute(self):
		return self.end_time.minute

class AppointmentRequest():
	def __init__(
		self,
		title: str,
		earliest_date: date, latest_date: date,
		earliest_hour: int, earliest_minute: int,
		latest_hour: int, latest_minute: int,
		duration_hours: int, duration_minutes: int,
		break_hours: int, break_minutes: int
	):
		self.title = title
		self.earliest_date = earliest_date
		self.latest_date = latest_date
		self.earliest_time = datetime.combine(earliest_date, time(earliest_hour, earliest_minute))
		self.latest_time = datetime.combine(latest_date, time(latest_hour, latest_minute))
		self.duration = timedelta(hours=duration_hours, minutes=duration_minutes)
		self.minimum_break = timedelta(hours=break_hours, minutes=break_minutes)

class SendAppointmentsMessage():
	pass

class NoAppointmentFound():
	pass

def _flatten(appointment_list):
	return [appointment for sublist in appointment_list for appointment in sublist]

def in_times(start: datetime, end: datetime, appointment_list):
	for app in appointment_list:
		if (
			app.end_time >= start.time() >= app.start_time
			or app.end_time >= end.time() >= app.start_time
		):
			return True
	return False

def within_requested_timeframe(start, end, request):
	return (
		request.earliest_time.time() <= start.time() <= request.latest_time.time()
		and request.earliest_time.time() <= end.time() <= request.latest_time.time()
	)

def compute_appointment(request: AppointmentRequest, existing_appointments: list) -> Appointment or NoAppointmentFound:
	occupied = _flatten(existing_appointments)

	for day in pandas.date_range(request.earliest_date, request.latest_date):
		day = day.date()
		appointments_for_day = [app for app in occupied if str(app.day) == str(day)]
		start_time = request.earliest_time
		end_time = start_time + request.duration

		while (within_requested_timeframe(start_time, end_time, request)):

			if start_time.time() in [app.end_time for app in appointments_for_day]:
				start_time += request.minimum_break
				end_time += request.minimum_break
				if not (within_requested_timeframe(start_time, end_time, request)):
					break

			elif not in_times(start_time, end_time, appointments_for_day):
				return Appointment(request.title, day, start_time.hour, start_time.minute, end_time.hour, end_time.minute)

			else:
				start_time += request.duration
				end_time += request.duration

	return NoAppointmentFound()
