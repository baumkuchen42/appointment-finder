import datetime

from gi.repository import Gtk, Handy

from .utils import get_formatted_date_from_calendar, Appointment, AppointmentRequest, select_current_date

from .appointment_widget import AppointmentWidget

@Gtk.Template(resource_path='/de/hszg/appointmentfinder/request_dialog.ui')
class RequestDialog(Gtk.Dialog):
	__gtype_name__ = 'RequestDialog'

	Handy.init()

	title_entry = Gtk.Template.Child()

	earliest_date_btn = Gtk.Template.Child()
	latest_date_btn = Gtk.Template.Child()
	earliest_date_popover = Gtk.Template.Child()
	latest_date_popover = Gtk.Template.Child()
	earliest_date_calendar = Gtk.Template.Child()
	latest_date_calendar = Gtk.Template.Child()

	earliest_hour_entry = Gtk.Template.Child()
	earliest_minute_entry = Gtk.Template.Child()
	latest_hour_entry = Gtk.Template.Child()
	latest_minute_entry = Gtk.Template.Child()

	duration_hours_entry = Gtk.Template.Child()
	duration_minutes_entry = Gtk.Template.Child()
	duration_hours_label = Gtk.Template.Child()
	break_hours_entry = Gtk.Template.Child()
	break_minutes_entry = Gtk.Template.Child()
	break_hours_label = Gtk.Template.Child()

	request_btn = Gtk.Template.Child()
	dialog_stack = Gtk.Template.Child()

	appointment_listbox = Gtk.Template.Child()

	appointment = None

	def __init__(self, parent, **kwargs):
		super().__init__(**kwargs)
		self.parent = parent
		self.set_transient_for(self.parent)
		self.set_attached_to(self.parent)
		self.set_modal(True)
		self.init_calendars()
		self.init_date_btns()
		self.connect_events()

	def init_calendars(self):
		select_current_date(self.earliest_date_calendar)
		select_current_date(self.latest_date_calendar)

	def init_date_btns(self, widget=None):
		self.earliest_date_btn.set_label(str(get_formatted_date_from_calendar(self.earliest_date_calendar)))
		self.latest_date_btn.set_label(str(get_formatted_date_from_calendar(self.latest_date_calendar)))

	def connect_events(self):
		self.earliest_date_btn.connect('clicked', self.launch_calendar, self.earliest_date_popover)
		self.latest_date_btn.connect('clicked', self.launch_calendar, self.latest_date_popover)
		self.earliest_date_calendar.connect('day-selected', self.init_date_btns)
		self.latest_date_calendar.connect('day-selected', self.init_date_btns)
		self.duration_hours_entry.connect('activate', self.update_hours_label, self.duration_hours_label)
		self.break_hours_entry.connect('activate', self.update_hours_label, self.break_hours_label)

		self.request_btn.connect('clicked', self.show_next_page)

	def launch_calendar(self, btn, calendar_popover):
		calendar_popover.show()

	def show_next_page(self, widget=None):
		active_page = self.dialog_stack.get_visible_child_name()
		if active_page == 'setup_page':
			self.send_request_to_server()
		elif active_page == 'answer_page':
			self.parent.save_and_show_appointment(self.appointment)
			self.destroy()
		elif active_page == 'error_page':
			self.destroy()

	def send_request_to_server(self):
		request = AppointmentRequest(
			self.title_entry.get_text(),
			get_formatted_date_from_calendar(self.earliest_date_calendar),
			get_formatted_date_from_calendar(self.latest_date_calendar),
			int(self.earliest_hour_entry.get_text()),
			int(self.earliest_minute_entry.get_text()),
			int(self.latest_hour_entry.get_text()),
			int(self.latest_minute_entry.get_text()),
			int(self.duration_hours_entry.get_text()),
			int(self.duration_minutes_entry.get_text()),
			int(self.break_hours_entry.get_text()),
			int(self.break_minutes_entry.get_text())
		)
		self.parent.client.send_appointment_request(request)
		self.destroy()

	def update_hours_label(self, entry, hours_label):
		hours = int(entry.get_text())
		if hours > 1:
			hours_label.set_text('hours')
		else:
			hours_label.set_text('hour')
