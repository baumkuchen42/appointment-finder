from gi.repository import Gtk

from .utils import Appointment

@Gtk.Template(resource_path='/de/hszg/appointmentfinder/appointment.ui')
class AppointmentWidget(Gtk.Box):
	__gtype_name__ = 'AppointmentWidget'

	title_label = Gtk.Template.Child()
	date_label = Gtk.Template.Child()
	start_time_label = Gtk.Template.Child()
	end_time_label = Gtk.Template.Child()

	def __init__(self, appointment: Appointment, **kwargs):
		super().__init__(**kwargs)
		self.title_label.set_text(appointment.title)
		self.date_label.set_text(str(appointment.day))
		self.start_time_label.set_text(str(appointment.start_time))
		self.end_time_label.set_text(str(appointment.end_time))
