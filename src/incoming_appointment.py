from gi.repository import Gtk, Handy

from .utils import Appointment

from .appointment_widget import AppointmentWidget

@Gtk.Template(resource_path='/de/hszg/appointmentfinder/incoming_appointment.ui')
class IncomingAppointmentDialog(Gtk.Dialog):
	__gtype_name__ = 'IncomingAppointmentDialog'

	Handy.init()

	main_listbox = Gtk.Template.Child()
	decline_btn = Gtk.Template.Child()
	accept_btn = Gtk.Template.Child()

	def __init__(self, parent, appointment: Appointment, **kwargs):
		super().__init__(**kwargs)

		self.parent = parent
		self.set_transient_for(self.parent)
		self.set_attached_to(self.parent)
		self.set_modal(True)

		self.appointment = appointment
		self.main_listbox.add(AppointmentWidget(self.appointment))
		self.connect_events()

	def connect_events(self):
		self.decline_btn.connect('clicked', self.close)
		self.accept_btn.connect('clicked', self.save_and_close)

	def close(self, widget=None):
		self.destroy()

	def save_and_close(self, widget=None):
		self.parent.save_and_show_appointment(self.appointment)
		self.destroy()
