import socket, time, pickle, traceback

from threading import Thread

from gi.repository import GLib

from .utils import AppointmentRequest, Appointment, SendAppointmentsMessage, NoAppointmentFound
from .server import AppointmentServer

class AppointmentClient:
	def __init__(self, ip_port_tuple, gui):
		'''
		Initializes the client socket and connects to the server with
		the given ip and port.
		Sets the attribute ip_port_tuple for later use in Client itself
		and for identification from the GUI.
		'''
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.socket.connect(ip_port_tuple)
		except ConnectionRefusedError:
			server = AppointmentServer(ip_port_tuple)
			Thread(target=server.start_listening, daemon=True).start()
			self.socket.connect(ip_port_tuple)
		self.ip_port_tuple = ip_port_tuple
		self.gui = gui
		Thread(target=self.listen, daemon=True).start()

	def send_appointment_request(self, request: AppointmentRequest):
		try:
			self.socket.send(pickle.dumps(request))
		except Exception:
			traceback.print_exc()

	def listen(self):
		while True:
			try:
				data = self.socket.recv(1024)
				data = pickle.loads(data)
				if type(data) == SendAppointmentsMessage:
					self.socket.send(pickle.dumps(self.gui.appointments))
				elif type(data) == Appointment:
					GLib.idle_add(self.gui.show_new_appointment_msg, data)
				elif type(data) == NoAppointmentFound:
					GLib.idle_add(self.gui.notify, 'Sorry', 'No suitable common appointment has been found.')
			except Exception:
				traceback.print_exc()

	def close_connection(self):
		self.socket.close()
