# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import date

from gi.repository import Gtk, Handy

from .utils import get_formatted_date_from_calendar, Appointment, is_valid_ip, is_valid_port, select_current_date
from .appointment_widget import AppointmentWidget
from .request_dialog import RequestDialog
from .client import AppointmentClient
from .incoming_appointment import IncomingAppointmentDialog

@Gtk.Template(resource_path='/de/hszg/appointmentfinder/window.ui')
class AppointmentfinderTask5Window(Gtk.ApplicationWindow):
	__gtype_name__ = 'AppointmentfinderTask5Window'

	Handy.init()

	new_window_btn = Gtk.Template.Child()
	continue_btn = Gtk.Template.Child()

	main_stack = Gtk.Template.Child()
	main_box = Gtk.Template.Child()
	setup_box = Gtk.Template.Child()

	port_entry = Gtk.Template.Child()
	ip_entry = Gtk.Template.Child()

	calendar = Gtk.Template.Child()
	calendar_popover = Gtk.Template.Child()
	date_label = Gtk.Template.Child()
	save_appointment_btn = Gtk.Template.Child()
	title_entry = Gtk.Template.Child()
	start_hour_entry = Gtk.Template.Child()
	end_hour_entry = Gtk.Template.Child()
	start_minute_entry = Gtk.Template.Child()
	end_minute_entry = Gtk.Template.Child()

	apppointments_listbox = Gtk.Template.Child()
	request_appointment_btn = Gtk.Template.Child()

	ip_port_tuple = ()
	appointments = []
	client = None

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.connect_events()

	def connect_events(self):
		self.continue_btn.connect('clicked', self.init_agent)
		self.new_window_btn.connect('clicked', self.launch_new_agent)
		self.calendar.connect('day-selected', self.show_calendar_popover)
		self.save_appointment_btn.connect('clicked', self.create_appointment)
		self.request_appointment_btn.connect('clicked', self.show_request_dialog)

	def init_agent(self, widget=None):
		ip = self.ip_entry.get_text()
		port = int(self.port_entry.get_text())
		if is_valid_ip(ip) and is_valid_port(port):
			self.ip_port_tuple = (ip, port)
			self.client = AppointmentClient(self.ip_port_tuple, self)
			self.main_stack.set_visible_child(self.main_box)
			select_current_date(self.calendar)

	def launch_new_agent(self, widget=None):
		win = AppointmentfinderTask5Window()
		win.show_all()

	def show_calendar_popover(self, calendar: Gtk.Calendar):
		self.date_label.set_text(str(get_formatted_date_from_calendar(calendar)))
		self.calendar_popover.show()

	def create_appointment(self, widget=None):
		appointment = Appointment(
			self.title_entry.get_text(),
			self.date_label.get_text(),
			int(self.start_hour_entry.get_text()),
			int(self.start_minute_entry.get_text()),
			int(self.end_hour_entry.get_text()),
			int(self.end_minute_entry.get_text())
		)

		self.save_and_show_appointment(appointment)

	def save_and_show_appointment(self, appointment: Appointment):
		self.appointments.append(appointment)
		self.apppointments_listbox.add(AppointmentWidget(appointment))
		self.apppointments_listbox.show_all()
		self.calendar_popover.hide()

	def show_request_dialog(self, widget=None):
		dialog = RequestDialog(self)
		dialog.show()

	def show_new_appointment_msg(self, appointment: Appointment):
		dialog = IncomingAppointmentDialog(self, appointment)
		dialog.show()

	def notify(self, text, further_explanation=''):
		msg = Gtk.MessageDialog(
			transient_for=self,
			message_type=Gtk.MessageType.INFO,
			buttons=Gtk.ButtonsType.OK,
			text=text
		)
		msg.format_secondary_text(further_explanation)
		msg.run()
		msg.destroy()
