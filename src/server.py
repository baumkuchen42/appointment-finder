import socket, selectors, types, time, traceback, pickle

from .utils import AppointmentRequest, compute_appointment, SendAppointmentsMessage

class AppointmentServer:
	appointments_list = []
	appointment_request = NotImplemented

	def __init__(self, ip_port_tuple):
		'''
		Sets a default timeout, a default selector and creates a socket for
		listening. Also sets port and gui attribute.
		'''
		self.timeout = 2 #default timeout, if no timeout needed, timeout is set to None
		self.default_selector = selectors.DefaultSelector()
		self.listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.ip, self.port = ip_port_tuple

	def start_listening(self):
		self.listening_socket.bind((self.ip, self.port))
		self.listening_socket.listen()
		print('listening on', (self.ip, self.port))
		self.listening_socket.setblocking(False)
		self.default_selector.register(self.listening_socket, selectors.EVENT_READ, data=None)
		self.server_loop()

	def server_loop(self):
		print('start server main loop')
		while True:
			events = self.default_selector.select(timeout=None)
			for key, mask in events:
				if key.data is None:
					self.accept_wrapper(key.fileobj)
				else:
					try:
						self.listen_to_client_signals(key, mask)
					except ConnectionResetError:
						print('One client closed its connection')
					except Exception:
						traceback.print_exc()
			time.sleep(.2)

	def accept_wrapper(self, socket):
		"""
		Accepts new client and defines data type and events for the connection.

		:param socket: the fileobject belonging to the key of the client who wants to connect to server
		"""
		conn, addr = self.listening_socket.accept()
		print('accepted connection from', addr)
		conn.setblocking(False)
		data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
		events = selectors.EVENT_READ | selectors.EVENT_WRITE
		try:
			self.default_selector.register(conn, events, data=data)
		except KeyError:
			pass

	def listen_to_client_signals(self, key, mask):
		"""
		Listens at the client connection for new messages and processes them.

		:param key: key of the client
		:param mask: mask of the client
		"""
		socket = key.fileobj
		data = key.data
		if mask & selectors.EVENT_READ: #if socket is ready to read
			recv_data = socket.recv(1024)  #1024 is the maximum amount of data to be received at once
			if not recv_data:
				print('closing connection to', data.addr)
				self.default_selector.unregister(socket)
				socket.close()
			else:
				self.parse_msg(recv_data)
				time.sleep(.5)

	def parse_msg(self, data):
		data = pickle.loads(data)
		if type(data) == AppointmentRequest:
			self.broadcast_msg(SendAppointmentsMessage())
			self.appointment_request = data
		elif type(data) == list:
			self.appointments_list.append(data)
			if len(self.appointments_list) == len(self.default_selector.select(timeout=self.timeout)):
				new_appointment = compute_appointment(self.appointment_request, self.appointments_list)
				self.broadcast_msg(new_appointment)
				self.appointment_request = None
				self.appointments_list = []
		time.sleep(0.01)

	def broadcast_msg(self, msg):
		events = self.default_selector.select(timeout=self.timeout)
		for key, mask in events:
			if key.data is not None:
				socket = key.fileobj
				socket.send(pickle.dumps(msg))

	def close_connections(self):
		'''
		Loops through all connected sockets to close them one by one.
		'''
		events = self.default_selector.select(timeout=self.timeout)
		for key, mask in events:
			if key.data is not None:
				socket = key.fileobj
				socket.close()
